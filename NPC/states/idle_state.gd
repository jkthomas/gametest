extends State

@export var idle_timer: Timer
@export var min_idle_time = 5
@export var max_idle_time = 10

signal stop_movement

func _ready() -> void:
	idle_timer.timeout.connect(func(): state_finished.emit())

func _enter_state(_optional_arguments) -> void:
	stop_movement.emit()
	idle_timer.wait_time = randi_range(min_idle_time, max_idle_time)
	idle_timer.start()

func _exit_state() -> void:
	idle_timer.stop()

