extends State

@export var attack_timer: Timer
@export var attack_cooldown = 2

signal stop_movement
signal target_unreachable(target: CharacterBody3D)

var navigation_agent: NavigationAgent3D
var target: CharacterBody3D

func _ready() -> void:
	set_physics_process(false)
	attack_timer.wait_time = attack_cooldown
	attack_timer.timeout.connect(func(): attack())

func _enter_state(_optional_arguments) -> void:
	stop_movement.emit()
	navigation_agent = _optional_arguments["navigation_agent"] as NavigationAgent3D
	target = _optional_arguments["target"] as CharacterBody3D
	set_physics_process(true)
	attack_timer.start()

func _exit_state() -> void:
	attack_timer.stop()
	set_physics_process(false)

func _physics_process(_delta) -> void:
	navigation_agent.target_position = target.position
	if not navigation_agent.distance_to_target() <= navigation_agent.target_desired_distance:
		target_unreachable.emit(target)
	
func attack():
	# TODO: Implement attack logic
	print('ATTACKED')
	attack_timer.start()
