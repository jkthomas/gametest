extends State

signal set_chase_speed
signal target_reached(target: CharacterBody3D)
signal set_walk_speed

var navigation_agent: NavigationAgent3D
var target: CharacterBody3D

func _ready() -> void:
	set_physics_process(false)

func _enter_state(_optional_arguments) -> void:
	navigation_agent = _optional_arguments["navigation_agent"] as NavigationAgent3D
	target = _optional_arguments["target"] as CharacterBody3D
	set_chase_speed.emit()
	set_physics_process(true)

func _exit_state() -> void:
	set_physics_process(false)
	set_walk_speed.emit()

func _physics_process(_delta) -> void:
	navigation_agent.target_position = target.position
	if navigation_agent.distance_to_target() <= navigation_agent.target_desired_distance:
		target_reached.emit(target)
	elif navigation_agent.is_navigation_finished():
		state_finished.emit()
