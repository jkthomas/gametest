extends State

var navigation_agent: NavigationAgent3D

func _ready() -> void:
	set_physics_process(false)

func _enter_state(_optional_arguments) -> void:
	navigation_agent = _optional_arguments["navigation_agent"] as NavigationAgent3D
	var target_destination_position = _optional_arguments["target_destination_position"] as Vector3
	set_physics_process(true)
	navigation_agent.target_position = target_destination_position

func _exit_state() -> void:
	set_physics_process(false)

func _physics_process(_delta) -> void:
	if navigation_agent.is_navigation_finished():
		state_finished.emit()
