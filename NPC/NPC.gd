extends CharacterBody3D

class_name NPC

@export var npc_params_controller: CharacterParamsController

@onready var detection_area = $DetectionArea
@onready var navigation_agent = $NavigationAgent3D
@onready var model = $Barbarian
@onready var finite_state_machine = $FiniteStateMachine
@onready var idle_state = $FiniteStateMachine/IdleState
@onready var patrol_state = $FiniteStateMachine/PatrolState
@onready var chase_state = $FiniteStateMachine/ChaseState
@onready var attack_state = $FiniteStateMachine/AttackState
@onready var run_to_target_state = $FiniteStateMachine/RunToTargetState

signal patrol_finished

var is_in_the_encounter_area = false
var anim_tree = null

var walk_blend_amount = 0
var run_blend_amount = 0
var blend_speed = 15

func set_to_idle():
	finite_state_machine.change_state(idle_state)

func _ready():
	anim_tree = model.get_node("AnimationTree") as AnimationTree

	# idle_state.stop_movement.connect(func(): velocity = Vector3.ZERO)
	# patrol_state.state_finished.connect(set_to_idle)
	# chase_state.state_finished.connect(set_to_idle)
	# chase_state.set_chase_speed.connect(func(): npc_params_controller.modify_speed(2))
	# chase_state.set_walk_speed.connect(func(): npc_params_controller.modify_speed(-2))
	# chase_state.target_reached.connect(func(target: CharacterBody3D): finite_state_machine.change_state(attack_state, {"navigation_agent": navigation_agent, "target": target}))
	# attack_state.target_unreachable.connect(func(target: CharacterBody3D): finite_state_machine.change_state(chase_state, {"navigation_agent": navigation_agent, "target": target}))
	# attack_state.stop_movement.connect(func(): velocity = Vector3.ZERO)
	# run_to_target_state.set_run_speed.connect(func(): npc_params_controller.modify_speed(2))
	# run_to_target_state.set_walk_speed.connect(func(): npc_params_controller.modify_speed(-2))
	# run_to_target_state.state_finished.connect(set_to_idle)
	# TODO: Add logic where NPC is idle after patrol and awaits instructions
	patrol_state.state_finished.connect(func(): patrol_finished.emit())
	finite_state_machine.change_state(patrol_state, {"navigation_agent": navigation_agent, "target_destination_position": Vector3(position.x + 1, position.y, position.z + 1)})

func subscribe_to_signals(navigate_to_position: Signal):
	navigate_to_position.connect(func(target_destination_position: Vector3): finite_state_machine.change_state(patrol_state, {"navigation_agent": navigation_agent, "target_destination_position": target_destination_position}))

func set_patrol_target(target_position: Vector3):
	finite_state_machine.change_state(run_to_target_state, {"navigation_agent": navigation_agent, "target_position": target_position})

func _physics_process(_delta):
	var destination = navigation_agent.get_next_path_position() - global_position
	rotation.y = lerp_angle(rotation.y, atan2(destination.x, destination.z), _delta * 5)
	navigation_agent.velocity = destination.normalized() * npc_params_controller.speed
	update_animation_tree(_delta)

func _on_navigation_agent_3d_velocity_computed(safe_velocity):
	if finite_state_machine.is_current_state(chase_state) or finite_state_machine.is_current_state(patrol_state) or finite_state_machine.is_current_state(run_to_target_state):
		velocity = safe_velocity
	move_and_slide()
	
func _on_detection_area_body_entered(body: Player):
	pass
	# TODO: Uncomment after testing
	# finite_state_machine.change_state(chase_state, {"navigation_agent": navigation_agent, "target": body})

func _on_detection_area_body_exited(body: Player):
	pass
	# TODO: Uncomment after testing
	# finite_state_machine.change_state(run_to_target_state, {"navigation_agent": navigation_agent, "target_position": body.position})

func update_animation_tree(_delta):
	if finite_state_machine.is_current_state(patrol_state):
		run_blend_amount = lerpf(run_blend_amount, 0, blend_speed * _delta)
		walk_blend_amount = lerpf(walk_blend_amount, 1, blend_speed * _delta)
	elif finite_state_machine.is_current_state(chase_state) or finite_state_machine.is_current_state(run_to_target_state):
		run_blend_amount = lerpf(run_blend_amount, 1, blend_speed * _delta)
		walk_blend_amount = lerpf(walk_blend_amount, 0, blend_speed * _delta)
	else:
		run_blend_amount = lerpf(run_blend_amount, 0, blend_speed * _delta)
		walk_blend_amount = lerpf(walk_blend_amount, 0, blend_speed * _delta)
	
	anim_tree["parameters/run/blend_amount"] = run_blend_amount
	anim_tree["parameters/walk/blend_amount"] = walk_blend_amount



func _on_hurtbox_hit_received():
	# TODO: Handle being hit
	print('Hit received')
