class_name State
extends Node

signal state_finished

func _enter_state(_optional_arguments: Dictionary) -> void:
	pass

func _exit_state() -> void:
	pass
