class_name FiniteStateMachine
extends Node

@export var state: State

func _ready():
	change_state(state)

func change_state(new_state: State, optional_arguments: Dictionary={}) -> void:
	if state is State and new_state != state:
		state._exit_state()
	new_state._enter_state(optional_arguments)
	state = new_state

func is_current_state(comparison_state: State) -> bool:
	return state == comparison_state
