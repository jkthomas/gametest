extends State

signal movement_velocity_computer(vector: Vector2)
signal running_stopped()

var player_params_controller: CharacterParamsController
var get_transform_basis: Callable

func _ready() -> void:
	set_physics_process(false)

func _enter_state(_optional_arguments) -> void:
	player_params_controller = _optional_arguments["player_params_controller"] as CharacterParamsController
	get_transform_basis = _optional_arguments["get_transform_basis"] as Callable
	set_physics_process(true)

func _exit_state() -> void:
	set_physics_process(false)

func _physics_process(_delta) -> void:
	if Input.is_action_just_pressed("run"):
		player_params_controller.modify_speed(2)
	if Input.is_action_just_released("run"):
		player_params_controller.modify_speed(-2)
	
	var input_dir = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	var direction = (get_transform_basis.call() * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		var movement_vector = Vector2(direction.x * player_params_controller.speed, direction.z * player_params_controller.speed)
		movement_velocity_computer.emit(movement_vector)
	else:
		state_finished.emit()
