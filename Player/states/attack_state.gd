extends State

@export var attack_timer: Timer

signal stop_movement
var toggle_hitbox: Callable

# TODO: This timer and hitbox are completely desynced - hitbox is turned on too late and last very long, depending on the times attack was done in a row
func _ready() -> void:
	attack_timer.timeout.connect(func(): state_finished.emit())

func _enter_state(_optional_arguments) -> void:
	toggle_hitbox = _optional_arguments["toggle_hitbox"] as Callable
	toggle_hitbox.call(true)
	stop_movement.emit()
	attack_timer.start()

func _exit_state() -> void:
	toggle_hitbox.call(false)
	attack_timer.stop()

