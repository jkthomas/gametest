extends State

signal stop_movement

func _enter_state(_optional_arguments) -> void:
	stop_movement.emit()

func _exit_state() -> void:
	pass

