extends BoneAttachment3D

@onready var hitbox = $Hitbox

func _ready():
	toggle_hitbox(false)

func toggle_hitbox(toggle_value: bool) -> void:
	hitbox.monitorable = toggle_value
