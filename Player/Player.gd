extends CharacterBody3D

class_name Player

const SPEED = 5.0
const JUMP_VELOCITY = 4.5

@export var player_params_controller: CharacterParamsController

@onready var cam_pivot = $CamPivot
@export var sensitivity = 0.5
@onready var model = $Knight
@onready var hitbox = $Knight/Rig/Skeleton3D/Sword_1H/Hitbox
@onready var finite_state_machine = $FiniteStateMachine
@onready var idle_state = $FiniteStateMachine/IdleState
@onready var run_state = $FiniteStateMachine/RunState
@onready var attack_state = $FiniteStateMachine/AttackState
@onready var sword_1h = $Knight/Rig/Skeleton3D/Sword_1H

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var anim_tree = null

var run_blend_amount = 0
var run_left_blend_amount = 0
var run_right_blend_amount = 0
var blend_speed = 15
var was_attacking = false

func freeze_movement():
	velocity = Vector3(0, velocity.y, 0)
	Input.mouse_mode = Input.MOUSE_MODE_HIDDEN

func unfreeze_movement():
	finite_state_machine.change_state(idle_state)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	anim_tree = model.get_node("AnimationTree") as AnimationTree

	idle_state.stop_movement.connect(func(): velocity = Vector3(0, velocity.y, 0))
	run_state.movement_velocity_computer.connect(func(movement_vector: Vector2): velocity = Vector3(movement_vector.x, velocity.y, movement_vector.y))
	run_state.state_finished.connect(func(): finite_state_machine.change_state(idle_state))
	attack_state.stop_movement.connect(func(): freeze_movement())
	attack_state.state_finished.connect(func(): unfreeze_movement())

func _input(event):
	if event is InputEventMouseMotion and Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		rotate_y(deg_to_rad(-event.relative.x * sensitivity))
		cam_pivot.rotate_x((deg_to_rad(-event.relative.y * sensitivity)))
		cam_pivot.rotation.x = clamp(cam_pivot.rotation.x, deg_to_rad(-90), deg_to_rad(45))

func get_transform_basis() -> Basis:
	return transform.basis

func _physics_process(_delta):
	if Input.is_action_just_pressed("toggle_mouse"):
		if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		else:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

	if Input.is_action_just_pressed("quit"):
		get_tree().quit()

	if Input.is_action_just_pressed("attack") and not finite_state_machine.is_current_state(attack_state):
		finite_state_machine.change_state(attack_state, {"toggle_hitbox": sword_1h.toggle_hitbox})
		anim_tree["parameters/attack/request"] = AnimationNodeOneShot.ONE_SHOT_REQUEST_FIRE
		
	if not is_on_floor():
		velocity.y -= gravity * _delta

	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		

	if Input.get_vector("move_left", "move_right", "move_up", "move_down") != Vector2.ZERO and finite_state_machine.is_current_state(idle_state):
		finite_state_machine.change_state(run_state, {"player_params_controller": player_params_controller, "get_transform_basis": get_transform_basis})
	
	update_animation_tree(_delta)
	move_and_slide()

func update_animation_tree(_delta):
	if finite_state_machine.is_current_state(idle_state):
		run_blend_amount = lerpf(run_blend_amount, 0, blend_speed * _delta)
		run_left_blend_amount = lerpf(run_left_blend_amount, 0, blend_speed * _delta)
		run_right_blend_amount = lerpf(run_right_blend_amount, 0, blend_speed * _delta)
	elif finite_state_machine.is_current_state(run_state):
		run_blend_amount = lerpf(run_blend_amount, 1, blend_speed * _delta)
		run_left_blend_amount = lerpf(run_left_blend_amount, 0, blend_speed * _delta)
		run_right_blend_amount = lerpf(run_right_blend_amount, 0, blend_speed * _delta)

	anim_tree["parameters/run/blend_amount"] = run_blend_amount
	anim_tree["parameters/run_left/blend_amount"] = run_left_blend_amount
	anim_tree["parameters/run_right/blend_amount"] = run_right_blend_amount
