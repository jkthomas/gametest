extends Area3D

class_name Hurtbox

signal hit_received

func _init():
	collision_layer = 0
	collision_mask = 2

func _ready():
	area_entered.connect(handle_area_entered)

func handle_area_entered(hitbox: Hitbox) -> void:
	if hitbox == null:
		return

	hit_received.emit()
