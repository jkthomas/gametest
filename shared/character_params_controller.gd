extends Node

class_name CharacterParamsController

@export var character_resource: Character

var _MIN_MODIFIER_VALUE: int = -5
var _MAX_MODIFIER_VALUE: int = 5

var health: int:
	get:
		return health

var damage: int:
	get:
		return damage + _damage_modifier

var _damage_modifier: int = 0

func modify_damage(modifier: int):
		if(modifier <= _MIN_MODIFIER_VALUE or modifier >= _MAX_MODIFIER_VALUE or modifier == 0):
			print('Damage modifier must be a value between -5 and 5, excluding 0. Provided value:', modifier)
		_damage_modifier = clamp(_damage_modifier + modifier, _MIN_MODIFIER_VALUE, _MAX_MODIFIER_VALUE)

var defense: int:
	get:
		return defense + _defense_modifier

var _defense_modifier: int = 0

func modify_defense(modifier: int):
		if(modifier <= _MIN_MODIFIER_VALUE or modifier >= _MAX_MODIFIER_VALUE or modifier == 0):
			print('Defense modifier must be a value between -5 and 5, excluding 0. Provided value:', modifier)
		_defense_modifier = clamp(_defense_modifier + modifier, _MIN_MODIFIER_VALUE, _MAX_MODIFIER_VALUE)

var speed: int:
	get:
		return speed + _speed_modifier

var _speed_modifier: int = 0

func modify_speed(modifier: int):
		if(modifier <= _MIN_MODIFIER_VALUE or modifier >= _MAX_MODIFIER_VALUE or modifier == 0):
			print('Speed modifier must be a value between -5 and 5, excluding 0. Provided value:', modifier)
		_speed_modifier = clamp(_speed_modifier + modifier, _MIN_MODIFIER_VALUE, _MAX_MODIFIER_VALUE)

var knockback_resistance: int:
	get:
		return knockback_resistance + _knockback_resistance_modifier

var _knockback_resistance_modifier: int = 0

func modify_knockback_resistance(modifier: int):
		if(modifier <= _MIN_MODIFIER_VALUE or modifier >= _MAX_MODIFIER_VALUE or modifier == 0):
			print('Knockback resistance modifier must be a value between -5 and 5, excluding 0. Provided value:', modifier)
		_knockback_resistance_modifier = clamp(_knockback_resistance_modifier + modifier, _MIN_MODIFIER_VALUE, _MAX_MODIFIER_VALUE)

func _ready():
	health = character_resource.base_health
	damage = character_resource.base_damage
	defense = character_resource.base_defense
	speed = character_resource.base_speed
	knockback_resistance = character_resource.base_knockback_resistance