extends Node3D

class_name Encounter

var characters: Array[NPC]
var faction: Globals.Factions
var area: EncounterArea

func _init(_origin_position: Vector3, character_scene: PackedScene, characters_number: int, size: Vector3, _faction: Globals.Factions = Globals.Factions.Neutral):
	position = _origin_position
	faction = _faction

	for n in characters_number:
		var character_instance = character_scene.instantiate()
		character_instance.position.x = character_instance.position.x  + randi_range(0, 2*n)
		character_instance.position.z = character_instance.position.z  + randi_range(0, 2*n)
		characters.append(character_instance)

	for character_instance in characters:
		add_child(character_instance)

	area = EncounterArea.new()
	var collision_shape = CollisionShape3D.new()
	collision_shape.shape = BoxShape3D.new()
	collision_shape.shape.size = size
	area.add_child(collision_shape)
	area.area_entered.connect(_on_encounter_entered)
	add_child(area)

func _on_encounter_entered(node: EncounterArea):
	# TODO: Work a way to send signal to area parent
	print('Other encounter entered', node)
