extends Resource

class_name Character

@export var name: String:
	get:
		return name

@export_range(10, 100, 10) var base_health: int = 10:
	get:
		return base_health

@export_range(1, 5) var base_damage: int = 1:
	get:
		return base_damage

@export_range(0, 5) var base_defense: int = 0:
	get:
		return base_defense

@export_range(1, 10) var base_speed: int = 5:
	get:
		return base_speed

@export_range(1, 10) var base_knockback_resistance: int = 5:
	get:
		return base_knockback_resistance

