extends Encounter

class_name DynamicEncounter

signal navigation_triggered

var target_destination_position: Vector3

func _init(_origin_position: Vector3, character_scene: PackedScene, characters_number: int, _target_destination_position, _faction: Globals.Factions = Globals.Factions.Neutral):
	super._init(_origin_position, character_scene, characters_number, Vector3(10, 3, 10), _faction)
	target_destination_position = _target_destination_position
	for character in characters:
		character.subscribe_to_signals(navigation_triggered)
		# TODO: If the target reached, do something else
		character.patrol_finished.connect(func(): navigate_to_position())

func _ready():
	navigate_to_position()

func _physics_process(delta):
	# TODO: Update that to move properly
	position = Vector3(position.x + 2*delta, position.y, position.z + 2*delta)

func navigate_to_position():
	navigation_triggered.emit(target_destination_position)
