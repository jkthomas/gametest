extends Encounter

class_name StaticEncounter

signal navigation_triggered

var _MIN_PATROLLING_DISTANCE = 10
var _MAX_PATROLLING_DISTANCE = 50

func _init(_origin_position: Vector3, character_scene: PackedScene, characters_number: int, _faction: Globals.Factions = Globals.Factions.Neutral):
	super._init(_origin_position, character_scene, characters_number, Vector3(10, 3, 10), _faction)
	for character in characters:
		character.subscribe_to_signals(navigation_triggered)
		character.patrol_finished.connect(func(): handle_character_action(character))
	area.body_entered.connect(_on_npc_entered)
	area.body_exited.connect(_on_npc_exited)

func handle_character_action(character: NPC):
	if character.is_in_the_encounter_area:
		var patrolling_position = Vector3(position.x + randi_range(_MIN_PATROLLING_DISTANCE, _MAX_PATROLLING_DISTANCE), position.y, position.z + randi_range(_MIN_PATROLLING_DISTANCE, _MAX_PATROLLING_DISTANCE))
		navigation_triggered.emit(patrolling_position)
	else:
		navigation_triggered.emit(position)

# TODO: Add check whether the npc is from the given encounter
func _on_npc_entered(npc: NPC):
	print('NPC entered')
	npc.is_in_the_encounter_area = true

func _on_npc_exited(npc: NPC):
	print('NPC exited')
	npc.is_in_the_encounter_area = false