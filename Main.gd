extends Node3D

func _ready():
	var npc = preload("res://NPC/NPC.tscn")

	var dynamic_encounter = DynamicEncounter.new(Vector3(-20, 0, -20), npc, 2, Vector3(20, 0, 30))

	var static_encounter = StaticEncounter.new(Vector3(10, 0, 10), npc, 3)

	add_child(dynamic_encounter)
	add_child(static_encounter)
